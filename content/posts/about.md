+++
title = "A propos"
date = "2022-11-26"
author = "EDM115"
cover = "code.png"
description = "Petite présentation de qui je suis"
+++

# Bonjour 👋

Je m'appelle Lussandre Lederrey, mais un peu partout je suis EDM115.

Je suis un jeune développeur, toujours en apprentissage, qui aime expérimenter avec différentes technologies pour toujours être au courant des dernières nouveautés et parfaire ma maîtrise des languages de programmation.

J'ai déjà un portfolio sur [mon site](https://edm115.eu.org), mais celui-là est plus minimaliste.

## Études 🎓

J'ai fait un Bac Général spé Maths et NSI au Lycée Saint-Paul de Vannes de 2019 à 2022  
Et maintenant je poursuit mes études avec un BUT Informatique à l'IUT de Vannes de 2022 à 2025

## Compétences 💪

Informatique :
- Python
- Java
- HTML
- CSS
- JavaScript / Node.js (un peu)
- SQL / MongoDB (en apprentissage)  

Langues :
- Français (natif)
- Anglais (avancé)
- Espagnol (débutant)  
  
Certifications :
- MongoDB Database Administrator (2022)
- PIX [667/768] (2022)

## Intérets 🤓

- Programmation
- Apprentissage
- Jeux vidéo
- Musique

## Contact 📢

Vous pouvez me contacter par mail [dev@edm115.eu.org](mailto:dev@edm115.eu.org) ou par teléphone [0667980504](tel:+33667980504)  
Je suis présent un peu partout, vous pouvez aller sur [mon site](https://edm115.eu.org) et regarder tout en bas, mais voilà mon [GitHub](https://github.com/EDM115) et mon [Telegram](https://t.me/EDM115)