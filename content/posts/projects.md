+++
title = "Projets"
date = "2022-11-26"
author = "EDM115"
cover = "Hero_Gradient.webp"
description = "Petite présentation de mes projets dans ce post"
+++

Je vais présenter simplement les 3 projets majeurs que j'ai fait. Tous sont open-source et disponibles sur mon [GitHub](https://github.com/EDM115)

> Unzip Bot

Un bot Telegram qui permet d'extraire n'importe quelle archive (protégée par mot de passe ou non) et de renvoyer son contenu à l'utilisateur. Utilisé par plus de 1,1k personnes, c'est certainement mon plus grand projet à ce jour. https://github.com/EDM115/unzip-bot

> Underrated Producers

Un site qui a pour but de mettre en avant des musiciens de talent sous-côtés. Lorsque j'ai commencé à gérer des canaux Telegram toujours plus populaires dédiés à la musique, j'ai remarqué que certains producers ont un talent incroyables, mais peinent à se faire connaître. C'est pourquoi j'ai lancé ce site (https://edm115.eu.org/underrated) pour les aider dans leur promotion. Avec quasiment 15k visiteurs par mois, on peut dire que ça marche 🥰

> WARP+ Unlimited Advanced

Un projet que je n'ai pas crée, mais auquel j'ai contribué. Le principe est simple : ajouter de manière légitime du crédit au VPN de CloudFlare (WARP). J'ai participé à une refonte du code et une gestion asynchrone des requêtes. https://github.com/TheCaduceus/WARP-UNLIMITED-ADVANCED